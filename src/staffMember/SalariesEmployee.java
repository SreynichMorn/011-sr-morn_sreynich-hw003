package staffMember;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

public class SalariesEmployee extends StaffMember {
    private double salary;
    private double bonus;
    public SalariesEmployee(int id, String name, String address ,double salary,double bonus) {
        super(id, name, address);
        this.salary=salary;
        this.bonus=bonus;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        System.out.println("---------------------------");

        return super.toString()+"\nSalary         :"+salary+"\nBonus          :"+bonus+"\nPay            :"+(this.pay()==-1?null:this.pay());
    }
    public double pay(){
        if(salary<0 || bonus<0 ){
            return -1;
        }
        else {
            return salary+bonus;
        }
    }


}
