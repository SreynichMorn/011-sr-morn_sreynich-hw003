package staffMember;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) throws IOException {
        Scanner obj=new Scanner(System.in);
        ArrayList<StaffMember> st=new ArrayList<StaffMember>();
        st.add(new Volunteer(1,"Nich","Takeo"));
        st.add(new HourlyEmloyee(2,"Anna","PP",12,44));
        st.add(new SalariesEmployee(3,"Phea","Kompot",-200,100));
        Collections.sort(st);
        for(StaffMember sm:st){
            System.out.println(sm);
        }
        int op = 0;int option = 0;
        do{
            System.out.println("========================================================================\n");
            System.out.print("1/.Add Employee\t\t");
            System.out.print("2/.Edit\t\t");
            System.out.print("3/.Remove\t\t");
            System.out.print("4/.Exit\n");
            System.out.print("=========================================================================\n");
            System.out.print("Choose Option (1-4):");
            option=obj.nextInt();
            switch (option){
                case 1:{

                        System.out.print("---- ------------------------------------------------------------------------\n");
                        System.out.print("1/. Volunteer\t\t");
                        System.out.print("2/. Hourly Emp\t\t");
                        System.out.print("3/. Salaries Emp\t\t");
                        System.out.print("4/. Back\n");
                        System.out.print("------------------------------------------------------------------------------\n");
                        System.out.print("Choose Option (1-4):");
                        op=obj.nextInt();
                        switch (op){
                            case 1:{
                                System.out.println("==================ADD VOLUNTEER================");
                                System.out.print("Input ID:");
                                int id=obj.nextInt();
                                System.out.print("Input Name:");
                                String name=obj.next();
                                System.out.print("Input Address:");
                                String address=obj.next();
                                Volunteer vol1=new Volunteer(id,name.substring(0,1).toUpperCase()+name.substring(1),address);
                                st.add(vol1);
                                Collections.sort(st);
                                for(StaffMember vv:st){
                                    System.out.println(vv);
                                }
                                System.in.read();
                            }break;
                            case 2:{
                                System.out.println("==================ADD HOURLY EMPLOYEE================");
                                System.out.print("Input ID:");
                                int id=obj.nextInt();
                                System.out.print("Input Name:");
                                String name=obj.next();
                                System.out.print("Input Address:");
                                String address=obj.next();
                                System.out.print("Input Hour Worked:");
                                double hourWorked=obj.nextDouble();
                                System.out.print("Input Rate:");
                                double rate=obj.nextDouble();
                                HourlyEmloyee hm=new HourlyEmloyee(id,name.substring(0,1).toUpperCase()+name.substring(1),address,hourWorked,rate);
                                st.add(hm);
                                Collections.sort(st);
                                for(StaffMember vv:st){
                                    System.out.println(vv);
                                }

                                System.in.read();
                            }break;
                            case 3:{
                                System.out.println("==================ADD Salaries Employee================");
                                System.out.print("Input ID:");
                                int id=obj.nextInt();
                                System.out.print("Input Name:");
                                String name=obj.next();
                                System.out.print("Input Address:");
                                String address=obj.next();
                                System.out.print("Input Salary:");
                                double salary=obj.nextDouble();
                                System.out.print("Input Bonus:");
                                double bonus=obj.nextDouble();
                                SalariesEmployee s=new SalariesEmployee(id,name.substring(0,1).toUpperCase()+name.substring(1),address,salary,bonus);
                                st.add(s);
                                Collections.sort(st);
                                for(StaffMember vv:st){
                                    System.out.println(vv);
                                }
                                System.in.read();
                            }break;
                            case 4:{
                                Collections.sort(st);
                                for(StaffMember vv:st){
                                    System.out.println(vv);
                                }
                                System.in.read();
                            }break;
                        }

                }break;

                case 2:{
                    System.out.println("=====================Edit Employee================");
                    Demo d=new Demo();
                    d.edit(st);
                    Collections.sort(st);
                    for(StaffMember sm:st){
                        System.out.println(sm);
                    }
                }break;
                case 3:{
                    System.out.println("=====================Remove Employee=================");
                    Demo d=new Demo();
                    d.remove(st);
                    Collections.sort(st);
                    for(StaffMember sm:st){
                        System.out.println(sm);
                    }

                }break;
                case 4:{
                    System.out.println("^^GOOD BYE^^");
                    System.exit(0);

                }break;
            }
        }while(option!=4);
    }
    Scanner obj=new Scanner(System.in);
    //    =================Function Update==================
    void edit(ArrayList<StaffMember> st) {

        int b = 0;
        int index = 0;
        System.out.print("Enter id for update : ");
        int value = obj.nextInt();
        for (StaffMember ss : st) {
//            ==========Update Volunteer=============
            if (ss instanceof Volunteer) {
                if (value == ss.getId()) {
                    System.out.print("Input ID:");
                    int id = obj.nextInt();
                    System.out.print("Input Name:");
                    String name = obj.next();
                    System.out.print("Input Address:");
                    String address = obj.next();
                    st.set(index, new Volunteer(id, name.substring(0,1).toUpperCase()+name.substring(1), address));
                    System.out.println("Update Successfully");
                    b = 1;
                }
                index++;
            }
//            =================Update Hourly Employee=================
            else if(ss instanceof HourlyEmloyee){
                if(value==ss.getId()){
                    System.out.print("Input ID:");
                    int id = obj.nextInt();
                    System.out.print("Input Name:");
                    String name = obj.next();
                    System.out.print("Input Address:");
                    String address = obj.next();
                    System.out.print("Input Hour Worked:");
                    double hourWorked=obj.nextDouble();
                    System.out.print("Input Rate:");
                    double rate=obj.nextDouble();
                    st.set(index,new HourlyEmloyee(id,name.substring(0,1).toUpperCase()+name.substring(1),address,hourWorked,rate));
                    System.out.println("Update Successfully");
                    b=2;
                }
                index++;
            }
//            =============Update Salaries Employee================
            else if(ss instanceof SalariesEmployee){
                if(value==ss.getId()){
                    System.out.print("Input ID:");
                    int id=obj.nextInt();
                    System.out.print("Input Name:");
                    String name=obj.next();
                    System.out.print("Input Address:");
                    String address=obj.next();
                    System.out.print("Input Salary:");
                    double salary=obj.nextDouble();
                    System.out.print("Input Bonus:");
                    double bonus=obj.nextDouble();
                    st.set(index,new SalariesEmployee(id,name.substring(0,1).toUpperCase()+name.substring(1),address,salary,bonus));
                    System.out.println("Update Successfully");
                    b=3;
                }
                index++;
            }
        }
        if(b==0){
            System.out.println("Update not successfully");
        }
    }

    //===================Function Remove=====================
    void remove(ArrayList<StaffMember>st){
        int b=0;
        System.out.print("Enter ID for Remove:");
        int idRemove=obj.nextInt();
        for(int i=0;i<st.size();i++) {
            if (idRemove == st.get(i).getId()) {
                st.remove(i);
                System.out.println("Delete Successfully");
                b=1;
            }
        }
        if(b==0){
            System.out.println("Delete not Successfully");
        }
    }
}

