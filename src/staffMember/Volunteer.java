package staffMember;

import java.util.ArrayList;
import java.util.Scanner;

public class Volunteer extends StaffMember {
    public Volunteer(){
        super();
    }
    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public String toString() {
        System.out.println("---------------------------");
        return super.toString();
    }

    public double pay(){
        return super.pay();
    }

}
