package staffMember;

import java.util.ArrayList;
import java.util.Scanner;

public class HourlyEmloyee extends StaffMember {
    private double hourWorked;
    private double rate;
    public HourlyEmloyee(int id, String name, String address,double hourWorked,double rate) {
        super(id, name, address);
        this.hourWorked=hourWorked;
        this.rate=rate;
    }

    @Override
    public String toString() {
        System.out.println("---------------------------");
        return super.toString()+"\nHourWorked     :"+hourWorked+"\nRate           :"+rate+"\nPay            :"+pay();
    }
    public double pay(){
        return hourWorked*rate;
    }


}
